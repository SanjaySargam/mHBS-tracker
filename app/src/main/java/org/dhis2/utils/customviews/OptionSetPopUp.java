package org.dhis2.utils.customviews;

import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;

import org.dhis2.App;
import org.dhis2.R;
import org.dhis2.data.forms.dataentry.fields.spinner.OptionSetView;
import org.dhis2.data.forms.dataentry.fields.spinner.SpinnerViewModel;
import org.dhis2.utils.optionset.OptionSetOptionsHandler;
import org.hisp.dhis.android.core.D2;
import org.hisp.dhis.android.core.option.Option;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import kotlin.Pair;
import timber.log.Timber;


public class OptionSetPopUp extends PopupMenu {

    private final D2 d2;
    private final CompositeDisposable disposable;
    private HashMap<String, Option> optionsMap;
    private final OptionSetOptionsHandler optionSetOptionsHandler;
    private final Context context;
    private final OptionSetView optionSetView;

    public OptionSetPopUp(Context context, View anchor, SpinnerViewModel model,
                          OptionSetView optionSetView) {
        super(context, anchor);
        this.context = context;
        this.optionSetView = optionSetView;
        d2 = ((App) context.getApplicationContext()).serverComponent().userManager().getD2();
        optionSetOptionsHandler = new OptionSetOptionsHandler(
                model.getOptionsToHide(),
                model.getOptionGroupsToShow(),
                model.getOptionGroupsToHide());
        setOnDismissListener(menu -> dismiss());
        setOnMenuItemClickListener(item -> {
            dismiss();
            Option selectedOption = optionsMap.get(item.getTitle().toString());
            optionSetView.onSelectOption(selectedOption);
            return true;
        });
        disposable = new CompositeDisposable();

        disposable.add(
                Single.fromCallable(() -> d2.optionModule().options()
                        .byOptionSetUid().eq(model.optionSet()))
                        .map(optionRepository -> {
                            Pair<List<String>, List<String>> handlerOptionsResult = optionSetOptionsHandler.handleOptions();
                            List<String> finalOptionsToHide = handlerOptionsResult.component1();
                            List<String> finalOptionsToShow = handlerOptionsResult.component2();

                            if (!finalOptionsToShow.isEmpty())
                                optionRepository = optionRepository
                                        .byUid().in(finalOptionsToShow);

                            if (!finalOptionsToHide.isEmpty())
                                optionRepository = optionRepository
                                        .byUid().notIn(finalOptionsToHide);

                            return optionRepository.blockingGet();
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                this::setOptions,
                                Timber::e
                        )
        );

    }

    public void setOptions(List<Option> options) {
        optionsMap = new HashMap<>();
        Collections.sort(options, (option1, option2) -> option1.sortOrder().compareTo(option2.sortOrder()));
        AlertDialog.Builder optionsDialog = new AlertDialog.Builder(context, R.style.DhisMaterialDialog);
        optionsDialog.setTitle("Choose option");
        optionsDialog.setAdapter(new DialogAdapter(context, R.layout.item_options_layout, options),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                Option selectedOption  = options.get(position);
                optionSetView.onSelectOption(selectedOption);
            }
        });
        optionsDialog.show();
    }

    private static class DialogAdapter extends ArrayAdapter<Option> {
        private final int resourseLayout;
        public DialogAdapter(@NonNull Context context, int resource, @NonNull List<Option> options) {
            super(context, resource, options);
            resourseLayout = resource;
        }
        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            View view = convertView;
            if (convertView == null){
                view = LayoutInflater.from(parent.getContext()).inflate(resourseLayout, parent, false);
            }
            Option option =  getItem(position);
            TextView optionTxt = view.findViewById(R.id.txtOptions);
            optionTxt.setSelected(true);
            optionTxt.setText(option.displayName());
            return view;
        }
    }

    @Override
    public void dismiss() {
        disposable.clear();
        super.dismiss();
    }
}